//
//  AppDelegate.h
//  Instal WebView
//
//  Created by Fabio Collini on 16/10/14.
//  Copyright (c) 2014 Instal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

